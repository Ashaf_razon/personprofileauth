import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:hive/hive.dart';
import 'package:person_profile_auth/ui/sign_up_screen.dart';
import 'package:person_profile_auth/ui_components/background.dart';
import 'home.dart';

const String USER_BOX_NAME = 'ownersBox';
const String KEY_U_NAME = "user_name";
const String KEY_U_MAIL = "user_mail";
const String KEY_U_PASS = "user_pass";

class LoginScreen extends StatefulWidget {
  static const String NAME_KEY_EMAIL = "email";
  static const String NAME_KEY_PASS = "pass";
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late String _userMail;
  late String _userPass;

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final String _errorMessage = "This field is required";
  final _formKey = GlobalKey<FormBuilderState>();
  late Box _usersBox;
  late String userGetPass;
  late String userGetMail;

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    _usersBox = Hive.box(USER_BOX_NAME);
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    //Hive.close(); //close all hive boxes
    super.dispose();
  }

  _getInfo() {
    userGetMail = _usersBox.get(KEY_U_MAIL).toString();
    userGetPass = _usersBox.get(KEY_U_PASS).toString();
    debugPrint('Login Info retrieved from box:' + userGetMail + " " + userGetPass);
  }

  Widget _buildEmailUser() {
    return FormBuilderTextField(
      name: LoginScreen.NAME_KEY_EMAIL,
      enableSuggestions: true,
      controller: emailController,
      textCapitalization: TextCapitalization.none,
      keyboardType: TextInputType.emailAddress,
      validator: (String? value) {
        if (value == null || value.length < 10) {
          return _errorMessage;
        }
        return null;
      },
      decoration: const InputDecoration(
          errorBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
          labelText: "Enter your E-mail",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          prefixIcon: Icon(Icons.mail)),
    );
  }

  Widget _buildPassUser() {
    return FormBuilderTextField(
      name: LoginScreen.NAME_KEY_PASS,
      controller: passwordController,
      validator: (String? value) {
        if (value == null || value.length < 3) {
          return _errorMessage;
        }
        return null;
      },
      decoration: const InputDecoration(
          errorBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
          labelText: "Enter your Password",
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          prefixIcon: Icon(Icons.password)),
      obscureText: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Background(
          child: FormBuilder(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: const Text(
                "LOGIN",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF2661FA),
                    fontSize: 30),
                textAlign: TextAlign.left,
              ),
            ),
            SizedBox(height: size.height * 0.03),
            Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.symmetric(horizontal: 40),
              child: _buildEmailUser(),
            ),
            SizedBox(height: size.height * 0.03),
            Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.symmetric(horizontal: 40),
              child: _buildPassUser(),
            ),
            SizedBox(height: size.height * 0.05),
            Container(
              alignment: Alignment.centerRight,
              margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: ElevatedButton(
                onPressed: () {
                  try {
                    if (_formKey.currentState != null) {
                      _formKey.currentState!.validate();
                      _formKey.currentState!.save();
                    }
                    _userMail = _formKey.currentState!
                        .fields[LoginScreen.NAME_KEY_EMAIL]!.value;
                    _userPass = _formKey
                        .currentState!.fields[LoginScreen.NAME_KEY_PASS]!.value;
                  } catch (E) {
                    debugPrint("Login Err: "+E.toString());
                    String er = E.toString();
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return const AlertDialog(
                            title: Text("Sorry!!!"),
                            content: Text("Box has already been closed, Try Restart the app."),
                          );
                        });
                  }
                  _getInfo();
                  //keyboard unfocus
                  FocusScope.of(context).unfocus();
                  if (userGetMail.compareTo(_userMail) == 0 &&
                      userGetPass.compareTo(_userPass) == 0) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const HomeScreen()));
                  } else {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return const AlertDialog(
                            title: Text("Sorry!!!"),
                            content: Text(
                                "User credential does not match, try again"),
                          );
                        });
                  }
                },
                style: ElevatedButton.styleFrom(
                  primary:
                      Colors.pinkAccent, //change background color of button
                  onPrimary: Colors.white, //change text color of button
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80),
                  ),
                  padding: const EdgeInsets.all(0),
                ),
                child: Container(
                  alignment: Alignment.center,
                  height: 50.0,
                  width: size.width * 0.5,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(80.0),
                      gradient: const LinearGradient(colors: [
                        Color.fromARGB(255, 245, 25, 164),
                        Color.fromARGB(255, 255, 177, 41)
                      ])),
                  padding: const EdgeInsets.all(0),
                  child: const Text(
                    "LOGIN",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: TextButton(
                child: const Text(
                  "Don't Have an Account? Sign up",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF2661FA)),
                ),
                  onPressed: (){
                  try{
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const RegisterScreen()));
                  }catch(E){
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return const AlertDialog(
                            title: Text("Sorry!!!"),
                            content: Text(
                                "Try, Restart app!!"),
                          );
                        });
                  }
                }
                ),
              )
          ],
        ),
      )),
    );
  }
}
