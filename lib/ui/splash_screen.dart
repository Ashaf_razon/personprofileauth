import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:person_profile_auth/ui/home.dart';
import 'package:person_profile_auth/ui/login_screen.dart';

const String USER_BOX_NAME = 'ownersBox';
const String KEY_U_NAME = "user_name";
const String KEY_U_MAIL = "user_mail";
const String KEY_U_PASS = "user_pass";

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<MyHomePage> {
  late Box _usersBox;
  late String userGetPass;
  late String userGetMail;

  @override
  void dispose() {
    //Hive.close(); //close all hive boxes
    super.dispose();
  }

  _getInfo() {
    userGetMail = _usersBox.get(KEY_U_MAIL).toString();
    userGetPass = _usersBox.get(KEY_U_PASS).toString();
    debugPrint(
        'Splash Info retrieved from box:' + userGetMail + " " + userGetPass);
  }

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    _usersBox = Hive.box(USER_BOX_NAME);
    // just using this code, with timer with stop if condition is reached
    Timer.periodic(const Duration(seconds: 2), (timer) {
      _getInfo();
      if ((userGetMail.isEmpty || userGetMail.compareTo("null") ==0 )&&
          (userGetPass.isEmpty || userGetPass.compareTo("null") == 0)){
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => const LoginScreen()));
      }else{
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => const HomeScreen()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        height: MediaQuery.of(context).size.height * 0.8,
        width: MediaQuery.of(context).size.width * 0.7,
        color: Colors.deepOrange,
        child: Image.asset('images/main.png'));
  }
}
