import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hive/hive.dart';

import 'login_screen.dart';

const String USER_BOX_NAME = 'ownersBox';
const String KEY_U_NAME = "user_name";
const String KEY_U_MAIL = "user_mail";
const String KEY_U_PASS = "user_pass";

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late final Box _usersBox;
  String userGetName = "";
  String userGetMail = "";
  late Size size;

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    _usersBox = Hive.box(USER_BOX_NAME);
    _getInfo();
  }

  @override
  void dispose() {
    //Hive.close(); //close all hive boxes
    super.dispose();
  }

  // Read info from people box
  _getInfo() {
    userGetName = _usersBox.get(KEY_U_NAME).toString();
    userGetMail = _usersBox.get(KEY_U_MAIL).toString();
    debugPrint('Info retrieved from box:' + userGetName + " " + userGetMail);
  }

  // Delete info from people box
  _deleteInfo() {
    _usersBox.delete(KEY_U_NAME);
    _usersBox.delete(KEY_U_MAIL);
    _usersBox.delete(KEY_U_PASS);
    print('Info deleted from box!');
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: HomeScreenUi(),
    );
  }

  Widget HomeScreenUi() {
    return Scaffold(
      backgroundColor: Colors.white70,
      appBar: AppBar(title: const Text("Profile")),
      body: Container(
          width: double.infinity,
          child: Column(
            children: <Widget>[
              Container(
                color: Colors.white,
                height: size.height * 0.2,
                width: size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Container(
                        margin: const EdgeInsets.all(5.0),
                        alignment: Alignment.topCenter,
                        padding: EdgeInsets.all(1.0),
                        child: Image.asset('images/person.png'),
                      ),
                    ),
                    Expanded(
                        flex: 8,
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: const EdgeInsets.all(10.0),
                                alignment: Alignment.topLeft,
                                height: size.height * 0.1,
                                child: Text('User name: '+userGetName),
                              ),
                              Container(
                                padding: const EdgeInsets.all(10.0),
                                alignment: Alignment.topLeft,
                                height: size.height * 0.1,
                                child: Text('Mail: '+userGetMail),
                              )
                            ],
                          ),
                        )
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 30),
                child: ElevatedButton(
                  onPressed: () {
                    _deleteInfo();
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const LoginScreen()));
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.pinkAccent,//change background color of button
                    onPrimary: Colors.white,//change text color of button
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80),
                    ),
                    padding: const EdgeInsets.all(0),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    width: size.width * 0.5,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(80.0),
                        gradient: const LinearGradient(
                            colors: [
                              Color.fromARGB(255, 245, 25, 164),
                              Color.fromARGB( 255, 255, 177, 41)
                            ]
                        )
                    ),
                    padding: const EdgeInsets.all(0),
                    child: const Text(
                      "Log out",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}