import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:person_profile_auth/ui_components/background.dart';
import 'login_screen.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

const String USER_BOX_NAME = 'ownersBox';
const String KEY_U_NAME = "user_name";
const String KEY_U_MAIL = "user_mail";
const String KEY_U_PASS = "user_pass";

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  static const String NAME_KEY_USER_NAME = "username";
  static const String NAME_KEY_EMAIL = "email" ;
  static const String NAME_KEY_PASS = "pass";

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late String _userUserName;
  late String _userMail;
  late String _userPass;

  final TextEditingController userNameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final String _errorMessage = "This field is required";
  final _formKey = GlobalKey<FormBuilderState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  late final Box _usersBox;

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    _usersBox = Hive.box(USER_BOX_NAME);
  }

  @override
  void dispose() {
    userNameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    Hive.close(); //close all hive boxes

    super.dispose();
  }

  _addInfo(String userName, String email, String pass) async {
    // Add info to people box
    // Storing key-value pair
    _usersBox.put(KEY_U_NAME, userName);
    _usersBox.put(KEY_U_MAIL, email);
    _usersBox.put(KEY_U_PASS, pass);

    debugPrint('Info added to box!');
  }

  Widget _buildUserName() {
    return FormBuilderTextField(
      name: RegisterScreen.NAME_KEY_USER_NAME,
      enableSuggestions: true,
      controller: userNameController,
      keyboardType: TextInputType.text,
      validator: (String? value) {
        if (value == null || value.length < 5){
          return _errorMessage;
        }
        return null;
      },
      decoration: const InputDecoration(
          errorBorder:
          OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
          labelText: "Enter your User-name",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          prefixIcon: Icon(Icons.person)),
    );
  }

  Widget _buildEmailUser() {
    return FormBuilderTextField(
      name: RegisterScreen.NAME_KEY_EMAIL,
      enableSuggestions: true,
      controller: emailController,
      textCapitalization: TextCapitalization.none,
      keyboardType: TextInputType.emailAddress,
      validator: (String? value) {
        if (value == null || value.length < 10){
          return _errorMessage;
        }
        return null;
      },
      decoration: const InputDecoration(
          errorBorder:
          OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
          labelText: "Enter your E-mail",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          prefixIcon: Icon(Icons.mail)),
    );
  }

  Widget _buildPassUser() {
    return FormBuilderTextField(
      name: RegisterScreen.NAME_KEY_PASS,
      controller: passwordController,
      validator: (String? value) {
        if (value == null || value.length < 3){
          return _errorMessage;
        }
        return null;
      },
      decoration: const InputDecoration(
          errorBorder:
          OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
          labelText: "Enter your Password",
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          prefixIcon: Icon(Icons.password)),
      obscureText: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      key: _scaffoldKey,
      body: Background(
        child: FormBuilder(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: const Text(
                  "REGISTER",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF2661FA),
                      fontSize: 30
                  ),
                  textAlign: TextAlign.left,
                ),
              ),

              SizedBox(height: size.height * 0.03),

              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.symmetric(horizontal: 40),
                child: _buildUserName(),
              ),

              SizedBox(height: size.height * 0.03),

              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.symmetric(horizontal: 40),
                child: _buildEmailUser(),
              ),

              SizedBox(height: size.height * 0.03),

              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.symmetric(horizontal: 40),
                child: _buildPassUser(),
              ),

              SizedBox(height: size.height * 0.05),

              Container(
                alignment: Alignment.centerRight,
                margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: ElevatedButton(
                  onPressed: () {
                    try{
                      debugPrint("Register: "+userNameController.text+" "+emailController.text+" "+passwordController.text);
                      if(_formKey.currentState != null) {
                        _formKey.currentState!.validate();
                        _formKey.currentState!.save();
                      }
                      _userUserName = _formKey.currentState!.fields[RegisterScreen.NAME_KEY_USER_NAME]!.value.toString();
                      _userMail = _formKey.currentState!.fields[RegisterScreen.NAME_KEY_EMAIL]!.value.toString();
                      _userPass = _formKey.currentState!.fields[RegisterScreen.NAME_KEY_PASS]!.value.toString();
                      debugPrint("Register V: " + _userUserName +" "+_userMail+" "+_userPass);
                      _addInfo(_userUserName, _userMail, _userPass);

                      if(_userUserName.isEmpty && _userMail.isEmpty && _userPass.isEmpty){
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return const AlertDialog(
                                title: Text("Sorry!!!"),
                                content: Text(
                                    "Try again"),
                              );
                            });
                      }else{
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => const LoginScreen()));
                      }
                    }catch(E){
                      debugPrint("Register Er: "+E.toString());
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.pinkAccent,//change background color of button
                    onPrimary: Colors.white,//change text color of button
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80),
                    ),
                    padding: const EdgeInsets.all(0),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    width: size.width * 0.5,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(80.0),
                        gradient: const LinearGradient(
                            colors: [
                              Color.fromARGB(255, 245, 25, 164),
                              Color.fromARGB( 255, 255, 177, 41)
                            ]
                        )
                    ),
                    padding: const EdgeInsets.all(0),
                    child: const Text(
                      "SIGN UP",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ),
              ),

              Container(
                alignment: Alignment.centerRight,
                margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: GestureDetector(
                  onTap: () => {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen()))
                  },
                  child: const Text(
                    "Already Have an Account? Sign in",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF2661FA)
                    ),
                  ),
                ),
              )
            ],
          ),
        )
      ),
    );
  }
}