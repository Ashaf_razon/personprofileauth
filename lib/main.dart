import 'package:flutter/material.dart';
import 'package:person_profile_auth/hive_files/account.dart';
import 'ui/splash_screen.dart';
import 'package:hive_flutter/hive_flutter.dart';

const String USER_BOX_NAME = 'ownersBox';
late Box _usersBox;

Future<void> main() async{
  await Hive.initFlutter();
  _usersBox = await Hive.openBox(USER_BOX_NAME);
  Hive.registerAdapter(PersonAccountAdapter());

  runApp(const MyApp());
}
