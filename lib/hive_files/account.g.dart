// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PersonAccountAdapter extends TypeAdapter<PersonAccount> {
  @override
  final int typeId = 1;

  @override
  PersonAccount read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PersonAccount(
      user_name: fields[0] as String,
      user_mail: fields[1] as String,
      user_pass: fields[2] as String,
    );
  }

  @override
  void write(BinaryWriter writer, PersonAccount obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.user_name)
      ..writeByte(1)
      ..write(obj.user_mail)
      ..writeByte(2)
      ..write(obj.user_pass);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PersonAccountAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
