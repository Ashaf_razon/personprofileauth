import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
part 'account.g.dart';

@HiveType(typeId: 1)
class PersonAccount {
  PersonAccount({required this.user_name, required this.user_mail, required this.user_pass});

  @HiveField(0)
  String user_name;

  @HiveField(1)
  String user_mail;

  @HiveField(2)
  String user_pass;
}
